import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Scrapper {
    public static void main(String[] args) throws Exception {
        String FILE_NAME = "myfile.txt";
        String url = "https://news.yandex.ru/computers.html";
        Document document = Jsoup.connect(url).get();
        String[] words = document.select("body").text().toLowerCase().replaceAll("[.,:()%©—«»]", "").split(" ");
        Map<String,Integer> dictionary = new HashMap<String, Integer>();


        dictionary = createDictionary(args, FILE_NAME);
        dictionary = getCountValuesInString(dictionary, words);
        printDictionary(dictionary);
    }

    static private Map<String,Integer> createDictionary(String[] args, String FILE_NAME){
        Map<String,Integer> dictionary = new HashMap<String, Integer>();
        if (args.length == 0){
            try {

                List<String> lines = Files.readAllLines(Paths.get(FILE_NAME), StandardCharsets.UTF_8);
                for(String line: lines){
                    dictionary.put(line.toLowerCase(), 0);
                }
            } catch(IOException ex){

                System.out.println(ex.getMessage() + " :Файл не найден");
            }

        } else {
            for (String arg : args) {
                dictionary.put(arg.toLowerCase(), 0);
            }
        }
        return dictionary;
    }


    static private Map<String,Integer> getCountValuesInString(Map<String,Integer> dictionary, String[] words){
        for (String key: dictionary.keySet()
                ) {
            int count = 0;
            for (String word: words)
                if (key.equals(word)) {
                    count += 1;
                }
            dictionary.put(key,count);
        }
        return dictionary;
    }


    static private void printDictionary(Map<String,Integer> dictionary){
        dictionary.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(System.out::println);
    }

}
